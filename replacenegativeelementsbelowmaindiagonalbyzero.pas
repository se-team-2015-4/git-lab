unit ReplaceNegativeElementsBelowMainDiagonalByZero;

interface
const
    DimensionRows = 9;
    DimensionColumns = 9;
    MinMatrixRangeValue = -21;
    MaxMatrixRangeValue = 23;

type
    Matrix = array [1..DimensionRows, 1..DimensionColumns] of Real;

procedure ReplaceNegativeElementsBelowMainDiagonalByZero(var matrix: Matrix);

implementation
procedure ReplaceNegativeElementsBelowMainDiagonalByZero(var matrix: Matrix);
var
    i, j: Integer;
begin
    for i := 1 to DimensionRows do
    begin
        for j := 1 to i-1 do
        begin
            matrix[i, j] := 0;
        end;
    end;
end;
end.
