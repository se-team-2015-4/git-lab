program Lab2;

uses
    Crt, FillAndWriteMatrix, GetNumberPositiveElementsOnSideAntiDiagonal, ReplaceNegativeElementsBelowMainDiagonalByZero;

var
    matrix1: Matrix;
begin
    FillAndWriteMatrix.FillMatrix(matrix1);
    FillAndWriteMatrix.WriteMatrix(matrix1);
    ReadLn;
    WriteLn(GetNumberPositiveElementsOnSideAntiDiagonal.GetNumberPositiveElementsOnSideAntiDiagonal(matrix1));
    ReadLn;
    ReplaceNegativeElementsBelowMainDiagonalByZero.ReplaceNegativeElementsBelowMainDiagonalByZero(matrix1);
    FillAndWriteMatrix.WriteMatrix(matrix1);
    ReadLn;
end.

