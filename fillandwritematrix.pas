unit FillAndWriteMatrix;

interface
const
    DimensionRows = 9;
    DimensionColumns = 9;
    MinMatrixRangeValue = -21;
    MaxMatrixRangeValue = 23;

type
    Matrix = array [1..DimensionRows, 1..DimensionColumns] of Real;

procedure FillMatrix(var matrix: Matrix);
procedure WriteMatrix(var matrix: Matrix);

implementation
procedure FillMatrix(var matrix: Matrix);
var
    i, j: Integer;
begin
    Randomize;
    for i := 1 to DimensionRows do
    begin
        for j := 1 to DimensionColumns do
        begin
            matrix[i, j] := Random*(MaxMatrixRangeValue - MinMatrixRangeValue) + MinMatrixRangeValue;
        end;
    end;
end;

procedure WriteMatrix(var matrix: Matrix);
var
    i, j: Integer;
begin
    for i := 1 to DimensionRows do
    begin
        for j := 1 to DimensionColumns do
        begin
            Write(matrix[i, j]:6:2);
        end;
        WriteLn;
    end;
end;
end.

