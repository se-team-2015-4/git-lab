unit GetNumberPositiveElementsOnSideAntiDiagonal;

interface
const
    DimensionRows = 9;
    DimensionColumns = 9;

type
    Matrix = array [1..DimensionRows, 1..DimensionColumns] of Real;

function GetNumberPositiveElementsOnSideAntiDiagonal(var matrix: Matrix): Integer;

implementation
function GetNumberPositiveElementsOnSideAntiDiagonal(var matrix: Matrix): Integer;
var
    i: Integer;
begin
    GetNumberPositiveElementsOnSideAntiDiagonal := 0;
    for i := 1 to DimensionRows do
    begin
        if matrix[i, i] >= 0 then
        begin
            GetNumberPositiveElementsOnSideAntiDiagonal := GetNumberPositiveElementsOnSideAntiDiagonal + 1;
        end;
    end;

end;
end.
